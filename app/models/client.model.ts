export class Client
{
	  public partnerId:string
	  public clientId:string;
	  public clientName: string;
	  public clientDescription: string;
	  public clientAddress: string;
	  public userId:string;
	  public password: string;
	  public filePath: string;
	  public emailAddress: string;
}