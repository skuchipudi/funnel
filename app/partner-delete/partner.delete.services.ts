import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';



@Injectable()
export class PartnerDeleteServices {
    private partner_delete_url =
         'http://localhost:8080/partnerservices/delete/';

    constructor(private _http: Http) { }

   
    
    deletePartner(partnerId: any) {
          console.log (this.partner_delete_url+partnerId);
         return this._http.get(this.partner_delete_url+partnerId)
            .map(res => res.json())
            .catch(this.handleError);
    }

    handleError(error: Response){
        console.error(error.json());
        return  Observable.throw(error.json().error || 'PartnerDeleteServices: Server Error');
    }

}
