import {Component, OnInit} from '@angular/core';
import {RegistrationService} from './registration.service';
import {RegistrationInfo} from './registrationinfo';

@Component({
    template: `
        <h1>Registrations</h1>
         <div>
         <i *ngIf="_isLoading"  class="fa fa-spinner fa-spin fa-3x"></i>
         </div>
         <table class="table table-striped table-hover ">
         <thead>
            <tr>
             <th>Partner Name</th>
             <th>Server Address</th>
             <th>User Id</th>
             <th>Remote File Path</th>
             <th>Local File Path</th>
             <th>File Name</th>
            </tr>
         </thead>
        <tbody>
            <tr *ngFor = "let registrations_list of _registrations_list">
              <td> {{ registrations_list.partnerName}}</td>
              <td> {{ registrations_list.serverAddress}}</td>
              <td> {{ registrations_list.userId}}</td>
              <td> {{ registrations_list.remoteFilePath}}</td>
              <td> {{ registrations_list.localFilePath}}</td>
              <td> {{ registrations_list.filename}}</td>
              </tr>
        </tbody>
        </table>  
        `,
    providers: [RegistrationService ],
 })

export class RegistrationsListComponent implements OnInit {
    _isLoading = true;
    _registrations_list: any; // Array of RegistrationInfo

    constructor(
            private _registrationService: RegistrationService )
            //private _routeParms: RouteParams){
            {}
    
    
    ngOnInit(){
       
       this._registrationService.getAllRegistrations().
            subscribe( _registrations_list => {
                this._isLoading =false;
                this. _registrations_list =  _registrations_list;
            });
    }      
}